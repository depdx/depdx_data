import pandas as pd


class Dataset(object):
    pass


class Evictions(Dataset):
    def __init__(self):
        self.schema = {
            "case_code": "object",
            "filed_date": "datetime64[ns]",
            "case_description": "object",
            "status": "category",
            "county": "category",
            "city": "object",
            "directional": "object",
            "zip": "object",
            "evicting_property_managers": "object",
            "evicting_landlords": "object",
            "evicting_lawyers": "object",
            "evicting_agents": "object",
            "first_appearance_date": "datetime64[ns]",
            "next_appearance_date": "datetime64[ns]",
            "last_appearance_date": "datetime64[ns]",
        }

        self.file_location = "https://storage.googleapis.com/depdx_data/evictions.json"


data_sets = {"evictions": Evictions}


def get_df(data_set) -> pd.DataFrame:
    data_set = data_sets[data_set]
    file_type = data_set().file_location.split(".")[-1]

    if file_type == "csv":
        df = pd.read_csv(data_set().file_location)
        df = df.astype(data_set().schema)

    elif file_type == "json":
        df = pd.read_json(data_set().file_location)
        df = df.astype(data_set().schema)

    else:
      raise Exception(f"file extension {file_type} not supported")

    return df