# Don't Evict Portland Data

Evictions affect the most vulnerable in our community. The goal of this project is to create a safe way for anyone to learn about the evictions happening in Portland Metro without exposing any sensitive data about tenants. 

## Data

Our public data sets are stored in GCS

| Name | Location | Description |
|------|----------|-------------|
| evictions  | [`gs://depdx_data/evictions.csv`](https://storage.googleapis.com/depdx_data/evictions.csv), [`gs://depdx_data/evictions.json`](https://storage.googleapis.com/depdx_data/evictions.json)| A flat file summary of our published eviction data, including nested arrays | 
| evictions_current_year  | [`gs://depdx_data/evictions_current_year.csv`](https://storage.googleapis.com/depdx_data/evictions_current_year.csv), [`gs://depdx_data/evictions_current_year.json`](https://storage.googleapis.com/depdx_data/evictions_current_year.json)| The evictionsd data set filtered to the current calendar year | 
 
This data can be downloaded using the hyperlinks above or imported to a variety of analytics tools.

### Google Sheets example

Google Sheets provides an `IMPORTDATA()` function, which can load our data directly into a google sheet. There are limits on the size of data that Google will allow you to import, but our `evictions_current_year.csv` data set is small enough (we'll see if it still works come December).

```
=IMPORTDATA("https://storage.googleapis.com/depdx_data/evictions_current_year.csv")
```


## Helpers

In addition we have created helpers that to help speed up the use of this data. We also welcome any contributions!

### pandas helper

This helper creates classes to easily build pandas data frames from our available data sets. Simply use the `get_df()` function and the named data set of your choice as in `get_df("evictions")`which returns a dataframe with the data from the 'evictions' data set.

This can be easily accessed via a jupyter notebook (we like using Google's [Colab](https://colab.research.google.com/drive/1FcTr1DhEWqzjC4h_-2dP7FATNcFqgDVx?usp=sharing) by cloning this repo 
```
!git clone https://gitlab.com/depdx/depdx_data.git
```
and importing the pandas_helper module
```python
from depdx_data.pandas_helper import get_df

df = get_df("evictions")
```
